<%--
  Created by IntelliJ IDEA.
  User: lqa
  Date: 2022/4/20
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录</title>
    <!-- 引入 layui.css -->
    <link rel="stylesheet" href="//unpkg.com/layui@2.6.8/dist/css/layui.css">

    <!-- 引入 layui.js -->
    <script src="//unpkg.com/layui@2.6.8/dist/layui.js"></script>
    <style>
        body {
            background-color: #F2F2F2;
        }

        .login-form {
            width: 400px;
            height: 330px;
            margin: 0 auto;
            margin-top: calc((50% - 330px) / 2)
        }

        .form {
            margin-top: 20px;
        }
    </style>
    <script>
        let canDoChangeVerify = true

        function changeVerify() {

            if (canDoChangeVerify) {
                canDoChangeVerify = false;
                document.getElementById("verify").src = 'verify?ts=' + new Date().getTime();
                setTimeout(() => {
                    canDoChangeVerify = true;
                }, 1500)
            }
            else{
                layer.msg('你的小手太快，停一停！', {icon: 5});
            }
        }
    </script>
</head>
<body>
<div class="layui-card login-form">
    <div class="layui-card-header layui-bg-blue">企业ERP进销存管理系统</div>
    <div class="layui-card-body">
        <form class="layui-form form" method="post" action="doLogin">
            <div class="layui-form-item">
                <label class="layui-form-label">用户名:</label>
                <div class="layui-input-block">
                    <input type="text" name="username" required lay-verify="required" placeholder="请输入用户名"
                           autocomplete="off"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">密码:</label>
                <div class="layui-input-block">
                    <input type="password" name="password" required lay-verify="required" placeholder="请输入密码"
                           autocomplete="off"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">验证码:</label>
                <div class="layui-input-block">
                    <input type="text" name="verify" required lay-verify="required" placeholder="请输入验证码"
                           autocomplete="off"
                           class="layui-input"
                           style="width:155px;float: left"
                    >
                    <image id="verify" src="verify" style="cursor: pointer;vertical-align: middle;float: right"></image>
                    <button style="margin-top:10px" type="button" onclick="changeVerify()"
                            class="layui-btn layui-btn-fluid layui-btn-xs layui-btn-primary layui-border-black">看不清，换一张
                    </button>

                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <input class="layui-btn layui-btn-fluid layui-btn-normal" type="submit" value="登录"/>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
