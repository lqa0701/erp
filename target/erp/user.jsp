<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>用户管理</title>
    <style>
        body, html {
            background: #EEEEEE;
        }

        .content {
            width: calc(100% - 240px);
            height: calc(100% - 150px);
            float: right;
            padding: 20px;
        }

        .table-card {
            padding: 10px;
        }

        .top{
            width: calc(100% - 200px);
            height: 50px;
            background: white;
            margin-left: 200px;
            line-height: 50px;
        }
    </style>
</head>
<body>
<%--布局--%>
<jsp:include page="template/layout.jsp"/>
<%--内容--%>
<div class="top">
    <span class="layui-breadcrumb breadcrumb"><a id="breadcrumbTitle">用户管理</a></span>
</div>
<div class="content">
    <div class="layui-panel table-card">
        <button class="layui-btn">新增</button>
        <table class="layui-table lay-skin='nob' ">
            <colgroup>
                <col width="150">
                <col width="200">
                <col>
            </colgroup>
            <thead>
            <tr>
                <th>用户名</th>
                <th>密码</th>
                <th>性别</th>
                <th>手机号</th>
                <th>邮箱</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <jsp:useBean id="list" scope="request" type="java.util.List"/>
            <c:forEach var="user" items="${list}">
                <tr>
                    <td>${user.username}</td>
                    <td>${user.password}</td>
                    <td>${user.sex}</td>
                    <td>${user.phone}</td>
                    <td>${user.email}</td>
                    <td>
                        <button type="button" class="layui-btn layui-btn-normal layui-btn-sm">
                            <i class="layui-icon">&#xe642;</i>
                        </button>
                        <button type="button" class="layui-btn layui-btn-danger layui-btn-sm">
                            <i class="layui-icon">&#xe640;</i>
                        </button>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
