<%--
  Created by IntelliJ IDEA.
  User: lqa
  Date: 2022/5/11
  Time: 15:11
  To change this template use File | Settings | File Templates.
  统一的模版文件
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <!-- 引入 layui.css -->
    <link rel="stylesheet" href="//unpkg.com/layui@2.6.8/dist/css/layui.css">

    <!-- 引入 layui.js -->
    <script src="//unpkg.com/layui@2.6.8/dist/layui.js"></script>
    <style>
        body, html {
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
        }

        .header {
            width: 100%;
            height: 60px;
            line-height: 60px;
        }

        .title {
            margin-left: 20px;
            font-size: 18px;
        }

        .logout {
            float: right;
            margin-right: 20px;
            margin-top: 10px;
        }

        .left {
            background: #3A3D48;
            height: calc(100% - 60px);
            width: 200px;
            float: left;
        }


        .breadcrumb {
            margin-left: 15px;
        }
    </style>
</head>
<body>
<%--header 导航栏--%>
<div class="header layui-bg-black">
    <span class="title">企业ERP经销存管理系统</span>
    <a class="layui-btn layui-btn-primary layui-border-orange logout" href="doLogout">退出登录</a>
</div>
<div class="left">
    <ul class="layui-nav layui-nav-tree layui-bg-black" lay-filter="test">
        <!-- 侧边导航: <ul class="layui-nav layui-nav-tree layui-nav-side"> -->

        <li class="layui-nav-item"><a href="index.jsp">首页</a></li>
        <li class="layui-nav-item"><a href="" >分类管理</a></li>
        <li class="layui-nav-item"><a href="" >库存管理</a></li>
        <li class="layui-nav-item"><a href="" >出库管理</a></li>
        <li class="layui-nav-item"><a href="" >入库管理</a></li>
        <li class="layui-nav-item"><a href="doUserList">用户管理</a></li>
        <li class="layui-nav-item"><a href="doLog">日志管理</a></li>
    </ul>

</div>


</body>
</html>
