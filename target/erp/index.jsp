<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>首页</title>
    <style>
        body, html {
            background: #EEEEEE;
        }

        .content {
            width: calc(100% - 240px);
            height: calc(100% - 190px);
            float: right;
            padding: 20px;
        }
        .top{
            width: calc(100% - 200px);
            height: 50px;
            background: white;
            margin-left: 200px;
            line-height: 50px;
        }
    </style>
</head>
<body>
<%--布局--%>
<jsp:include page="template/layout.jsp"/>
<%--内容--%>
<div class="top">
    <span class="layui-breadcrumb breadcrumb"><a id="breadcrumbTitle">首页</a></span>
</div>
<div class="content">
    <h1>欢迎使用系统！</h1>
    <h1>用户名：${user.username}</h1>
    <h1>密码：${user.password}</h1>
    <h1>性别：${user.sex}</h1>
    <h1>手机号：${user.phone}</h1>
    <h1>邮箱：${user.email}</h1>
</div>
</body>
</html>
