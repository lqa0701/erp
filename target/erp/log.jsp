<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>日志管理</title>
    <style>
        body, html {
            background: #EEEEEE;
        }

        .content {
            width: calc(100% - 240px);
            height: calc(100% - 150px);
            float: right;
            padding: 20px;
        }

        .table-card {
            padding: 10px;
        }

        .top {
            width: calc(100% - 200px);
            height: 50px;
            background: white;
            margin-left: 200px;
            line-height: 50px;
        }
    </style>
</head>
<body>
<%--布局--%>
<jsp:include page="template/layout.jsp"/>
<%--内容--%>
<div class="top">
    <span class="layui-breadcrumb breadcrumb"><a id="breadcrumbTitle">用户管理</a></span>
</div>
<div class="content">
    <div class="layui-panel table-card">
        <table class="layui-table lay-skin='nob' ">
            <colgroup>
                <col width="150">
                <col width="200">
                <col>
            </colgroup>
            <thead>
            <tr>
                <th>用户名</th>
                <th>时间</th>
                <th>ip</th>
                <th>资源</th>
                <th>浏览器</th>
                <th>类型</th>
            </tr>
            </thead>
            <tbody>
            <jsp:useBean id="list" scope="request" type="java.util.List"/>
            <c:forEach var="log" items="${list}">
                <tr>
                    <td>${log.username}</td>
                    <td>${log.time}</td>
                    <td>${log.ip}</td>
                    <td>${log.url}</td>
                    <td>${log.browser}</td>
                    <td>${log.type}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div id="test1"></div>

    </div>
</div>
<script>
    layui.use('laypage', function () {
        var laypage = layui.laypage;

        //执行一个laypage实例
        laypage.render({
            elem: 'test1' //注意，这里的 test1 是 ID，不用加 # 号
            , count: ${count} //数据总数，从服务端得到
            , curr: ${page}
            ,layout:['count','prev', 'page', 'next']
            ,jump: function(obj, first){
                //obj包含了当前分页的所有参数，比如：
                console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit); //得到每页显示的条数
                //首次不执行
                if(!first){
                    //do something
                    window.location.href="doLog?page="+obj.curr+"&size="+obj.limit
                }
            }
        });
    });
</script>
</body>
</html>
