<%--
  Created by IntelliJ IDEA.
  User: lqa
  Date: 2022/5/11
  Time: 14:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>错误信息</title>
    <!-- 引入 layui.css -->
    <link rel="stylesheet" href="//unpkg.com/layui@2.6.8/dist/css/layui.css">

    <!-- 引入 layui.js -->
    <script src="//unpkg.com/layui@2.6.8/dist/layui.js"></script>
    <style>
        .box{
            width: 400px;
            height: 330px;
            margin: calc((50% - 330px) / 2) auto;
        }
        body,html{
            width: 100%;
            height: 100%;
            background: #EEEEEE;
        }
        .back-btn{
            margin-top: 180px;
        }
        .content{
            font-size: 16px;
        }
    </style>
</head>
<body>
<div class="layui-card box">
    <div class="layui-card-header layui-bg-red">错误提示</div>
    <div class="layui-card-body content">
        <%--        1.展示错误信息--%>
        登录超时，请重新登录！
        <%--        2.返回上一级的按钮的url--%>
        <a href="login.jsp" class="layui-btn layui-btn-fluid  layui-btn-primary layui-border-red back-btn">返回登录</a>

    </div>
</div>
</body>
</html>
