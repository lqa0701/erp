package com.erp.controller.user;

import com.erp.bean.User;
import com.erp.dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Author lqa
 * @Date 2022-05-11 16:09:09
 */
public class ListUserServlet extends HttpServlet {
    UserDao userDao = new UserDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> list = userDao.listQuery(0,10);
        req.setAttribute("list",list);
        req.getRequestDispatcher("user.jsp").forward(req,resp);
    }
}