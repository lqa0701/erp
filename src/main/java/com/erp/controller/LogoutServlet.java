package com.erp.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @Author lqa
 * @Date 2022-05-18 15:20:23
 */
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 退出登录
        // 1.获取session对象
        HttpSession session = req.getSession();
        // 2.清楚登录状态/把登录标识符设置为false
        session.setAttribute("isLogin",false);
        // 3.跳转回登录页面
        resp.sendRedirect(req.getContextPath()+"/login.jsp");
    }
}