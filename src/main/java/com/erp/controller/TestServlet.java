package com.erp.controller;

import com.erp.bean.User;
import com.erp.dao.UserDao;
import com.erp.utils.JDBCUtils;
import com.erp.utils.PropertiesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 * @Author lqa
 * @Date 2022-04-13 16:07:52
 */
public class TestServlet extends HttpServlet {
    Logger logger = LoggerFactory.getLogger(TestServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("info1");
        logger.warn("warn");
        logger.error("error");
        logger.debug("debug");

        resp.getWriter().println("hello world");
        UserDao userDao = new UserDao();
        List<User> users = userDao.listQuery(0,10);
        System.out.println(users.toString());
        System.out.println(111111);
    }

}