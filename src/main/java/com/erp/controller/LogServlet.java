package com.erp.controller;

import com.erp.bean.Log;
import com.erp.service.LogService;
import org.apache.commons.lang3.ObjectUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Author lqa
 * @Date 2022-05-18 16:19:48
 */
public class LogServlet extends HttpServlet {
    LogService logService = new LogService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int page = ObjectUtils.anyNotNull(req.getParameter("page")) ? Integer.parseInt(req.getParameter("page")) - 1 : 0;
        int size = ObjectUtils.anyNotNull(req.getParameter("size")) ? Integer.parseInt(req.getParameter("size")) : 10;
        List<Log> list = logService.listQuery(page, size);
        req.setAttribute("list", list);
        req.setAttribute("page", ObjectUtils.anyNotNull(req.getParameter("page")) ? Integer.parseInt(req.getParameter("page"))  : 1);
        req.setAttribute("count", logService.countLog());
        req.getRequestDispatcher("log.jsp").forward(req, resp);
    }
}