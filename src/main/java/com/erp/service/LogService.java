package com.erp.service;

import com.erp.bean.Log;
import com.erp.dao.LogDao;

import java.util.List;

/**
 * @Author lqa
 * @Date 2022-05-11 16:38:10
 */
public class LogService {
    LogDao logDao = new LogDao();

    /* *
     * @author lqa
     * @name 保存日志
     * @time 16:38 2022/5/11
     * @param log:
     * @return boolean
     **/
    public boolean saveLog(Log log) {
        return logDao.saveLog(log);
    }

    /* *
     * @author lqa
     * @name 获取列表
     * @time 16:32 2022/5/18
     * @param page:
     * @param size:
     * @return java.util.List<com.erp.bean.Log>
     **/
    public List<Log> listQuery(int page, int size) {
        return logDao.listQuery(page, size);
    }

    /* *
     * @author lqa
     * @name 获取总数
     * @time 16:32 2022/5/18
     * @return java.lang.Integer
     **/
    public Integer countLog(){
        return logDao.countLog();
    }
}