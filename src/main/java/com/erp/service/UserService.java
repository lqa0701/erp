package com.erp.service;

import com.erp.bean.User;
import com.erp.dao.UserDao;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

/**
 * @Author lqa
 * @Date 2022-04-20 15:59:16
 */
public class UserService {
    UserDao userDao = new UserDao();

    /* *
     * @author lqa
     * @name 登录
     * @time 15:59 2022/4/20
     * @param username: 账号
     * @param password: 密码
     **/
    public void login(String username, String password, HttpServletRequest request) throws Exception {
        // 实现登录逻辑
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            throw new Exception("用户名或密码不能为空");
        }
        User user = userDao.getUserByUsername(username);
        // 用户是否存在
        if (user == null) {
            throw new Exception("用户不存在");
        }
        // 密码是否正确
        if (!user.getPassword().equals(password)) {
            throw new Exception("密码错误");
        }
        // 设置用户session
        HttpSession session = request.getSession();
        // 登录标识符
        session.setAttribute("isLogin", true);
        // 存入用户信息
        session.setAttribute("user", user);
        // 存入登录时间
        session.setAttribute("time", System.currentTimeMillis());


    }
}