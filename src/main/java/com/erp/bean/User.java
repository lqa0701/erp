package com.erp.bean;

/**
 * @Author lqa
 * @Date 2022-04-20 14:22:34
 */
public class User {
    private Integer id;
    private String username;
    private String password;
    private String phone;
    private String email;
    private String sex;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String toString(){
        return "id:"+id+"\n"+
                "username:"+username+"\n"+
                "password:"+password+"\n"+
                "phone:"+phone+"\n"+
                "email:"+email+"\n";
    }
}