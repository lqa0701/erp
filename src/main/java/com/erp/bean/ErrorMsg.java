package com.erp.bean;

/**
 * 统一异常实体类
 * @Author lqa
 * @Date 2022-05-11 14:33:48
 */
public class ErrorMsg {
    // 错误信息内容
    String message;

    // 返回上一级的页面
    String returnPage;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReturnPage() {
        return returnPage;
    }

    public void setReturnPage(String returnPage) {
        this.returnPage = returnPage;
    }


}