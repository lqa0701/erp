package com.erp.bean;

import java.time.LocalDateTime;

/**
 * @Author lqa
 * @Date 2022-05-11 16:21:24
 */
public class Log {
    Integer id;
    // 用户名
    String username;
    // 访问时间
    LocalDateTime time;
    // 访问者IP
    String ip;
    // 访问的资源
    String url;
    // 访问者使用的浏览器
    String browser;
    // 类型 10-登录 20-操作
    Integer type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}