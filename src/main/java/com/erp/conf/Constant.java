package com.erp.conf;

import java.util.Arrays;
import java.util.List;

/**
 * 项目静态变量
 *
 * @Author lqa
 * @Date 2022-05-18 14:58:47
 */
public class Constant {

    // 不需要鉴权的请求
    public static final List<String> notAuthResource = Arrays.asList(
            "login.jsp", "doLogin", "verify","logout.jsp"
    );

    // 登录有效期
    public static final Long loginExpire = 60*60*24L;// 秒


}