package com.erp.dao;
import java.time.LocalDateTime;

import com.erp.bean.Log;
import com.erp.bean.User;
import com.erp.utils.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author lqa
 * @Date 2022-05-11 16:24:51
 */
public class LogDao {

    private final JDBCUtils jdbcUtils = new JDBCUtils();

    /* *
     * @author lqa
     * @name 保存日志
     * @time 16:37 2022/5/11
     * @param log:
     * @return boolean
     **/
    public boolean saveLog(Log log) {
        boolean saveRet = false;
        try {
            Connection connection = jdbcUtils.getConnection();
            if (connection != null) {
                String sql = "insert into log(username,time,url,ip,browser,type) value (?,?,?,?,?,?)";

                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, log.getUsername());
                preparedStatement.setObject(2, log.getTime());
                preparedStatement.setString(3, log.getUrl());
                preparedStatement.setString(4, log.getIp());
                preparedStatement.setString(5, log.getBrowser());
                preparedStatement.setInt(6, log.getType());

                int ret = preparedStatement.executeUpdate();
                if (ret > 0) {
                    saveRet = true;
                }
                jdbcUtils.closeResource(connection, preparedStatement,null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return saveRet;
    }

    /* *
     * @author lqa
     * @name 列表查询
     * @time 16:18 2022/5/18
     * @param page:
     * @param size:
     * @return java.util.List<com.erp.bean.Log>
     **/
    public List<Log> listQuery(int page, int size) {
        List<Log> list = new ArrayList<>();

        try {
            PreparedStatement pstm = null;
            ResultSet rs = null;
            Connection connection = jdbcUtils.getConnection();
            if (connection != null) {
                String sql = "select * from log limit ?,?";
                Object[] params = {page, size};
                rs = jdbcUtils.execute(connection, sql, params, rs, pstm);
                while (rs.next()) {
                    Log log = new Log();
                    log.setId(rs.getInt("id"));
                    log.setUsername(rs.getString("username"));
                    log.setTime( rs.getObject("time",LocalDateTime.class));
                    log.setIp(rs.getString("ip"));
                    log.setUrl(rs.getString("url"));
                    log.setBrowser(rs.getString("browser"));
                    log.setType(rs.getInt("type"));
                    list.add(log);
                }
                jdbcUtils.closeResource(connection, pstm, rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public int countLog(){
        int size = 0;
        try {
            PreparedStatement pstm = null;
            ResultSet rs = null;
            Connection connection = jdbcUtils.getConnection();
            if (connection != null) {
                String sql = "select count(*) as count from log";
                rs = jdbcUtils.execute(connection, sql, null, rs, pstm);
                while (rs.next()) {
                    size = rs.getInt("count");
                }
                jdbcUtils.closeResource(connection, pstm, rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return size;
    }
}