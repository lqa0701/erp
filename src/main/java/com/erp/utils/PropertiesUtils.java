package com.erp.utils;

import java.io.InputStream;
import java.util.Properties;

/* *
 * @author lqa
 * @name 获取配置的工具栏
 * @time 16:14 2022/4/13
 **/
public class PropertiesUtils {

    public String getProperties(String key) {
        try {
            // 通过类加载器去获取一个输入流
            InputStream is = this.getClass().getClassLoader().getResourceAsStream("application.properties");
            // 创建一个Properties对象
            Properties p = new Properties();
            p.load(is);
            // 获取配置文件中的值
            return p.getProperty(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}