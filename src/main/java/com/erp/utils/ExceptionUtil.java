package com.erp.utils;

import com.erp.bean.ErrorMsg;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 错误信息处理的工具
 * @Author lqa
 * @Date 2022-05-11 14:57:25
 */
public class ExceptionUtil {

    public void error(String errorMsg, String returnPage, ServletContext context,
                      HttpServletRequest req, HttpServletResponse resp
                      ) throws ServletException, IOException {
//      1.创建错误信息实体类
        ErrorMsg err = new ErrorMsg();
        err.setMessage(errorMsg);
        err.setReturnPage(returnPage);
//      2.传递参数
        req.setAttribute("error",err);
//      3.请求转发
        context.getRequestDispatcher("/error.jsp").forward(req,resp);



    }
}