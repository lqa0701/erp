package com.erp.utils;

import java.sql.*;

/* *
 * @author lqa
 * @name 数据库工具类
 * @time 16:25 2022/4/13
 **/
public class JDBCUtils {

    PropertiesUtils propertiesUtils = new PropertiesUtils();

    // 1.获取数据连接配置信息
    // 数据库驱动字符串
    // 5.7 com.mysql.jdbc.Driver  8.0 com.mysql.cj.jdbc.Driver
    private final String driver = "com.mysql.jdbc.Driver";
    // 数据库连接字符串
    private String url;
    // 数据库用户名
    private String username;
    // 数据库密码
    private String password;

    public JDBCUtils() {
        url = propertiesUtils.getProperties("db.host");
        username = propertiesUtils.getProperties("db.username");
        password = propertiesUtils.getProperties("db.password");
        System.out.println(url + "\n" + username + "\n" + password);
    }
    // 2.获取数据库连接

    public Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    // 2.查询的公共函数
    public ResultSet execute(Connection connection, String sql, Object[] params, ResultSet resultSet, PreparedStatement preparedStatement) {
        try {
            preparedStatement = connection.prepareStatement(sql);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    preparedStatement.setObject(i + 1, params[i]);
                }
            }

            resultSet = preparedStatement.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultSet;

    }

    // 3.增删改的公共函数
    public int execute(Connection connection, String sql, Object[] params, PreparedStatement preparedStatement) {
        int effectRows = 0;
        try {
            preparedStatement = connection.prepareStatement(sql);

            for (int i = 0; i < params.length; i++) {
                preparedStatement.setObject(i + 1, params[i]);
            }

            effectRows = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return effectRows;

    }

    // 4.释放资源
    public void closeResource(Connection connection, PreparedStatement preparedStatement, ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
                // GC回收
                resultSet = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
                // GC回收
                preparedStatement = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (connection != null) {
            try {
                connection.close();
                // GC回收
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }


}